import React, { useState, useEffect } from "react";
import * as firebase from "firebase";
import Loading from "../../components/Loading";
import UserGuest from "./UserGuest";
import UserLogged from "./UserLogged";

export default function MyAccount() {
  const [login, setLogin] = useState(null);

  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      //Si user = false/null/undefined, hará el setLogin para decir que el usuario no está logueado
      // :  --> de lo contrario significará que sí está logueado.
      !user ? setLogin(false) : setLogin(true);
    });
  }, []);

  if (login == null) {
    return <Loading isVisible={true} text="Cargando..." />;
  }
  return login ? <UserLogged /> : <UserGuest />;

  /*if (login) {
    //si login existe
    return (
      <View>
        <Text>Usuario logueado</Text>
      </View>
    );
  } else {
    return (
      <View>
        <Text>Usuario no logueado</Text>
      </View>
    );
  }*/
}
