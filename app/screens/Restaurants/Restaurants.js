import React, { useState, useEffect } from "react";
import { StyleSheet, View, Text } from "react-native";
import { Button } from "react-native-elements";
import * as firebase from "firebase";

export default function Restaurantes(props) {
  const { navigation } = props;
  //estado usuario
  const [user, setUser] = useState(null);
  useEffect(() => {
    firebase.auth().onAuthStateChanged((userInfo) => {
      setUser(userInfo);
    });
  }, []);

  return (
    <View style={styles.viewBody}>
      <Text>Estamos en Restaurantes</Text>
      {user && <AddRestaurantButton navigation={navigation} />}
    </View>
  );
}

function AddRestaurantButton(props) {
  const { navigation } = props;
  return (
    <Button
      title="Añadir Restaurante"
      buttonStyle={styles.btnAddRestaurant}
      titleStyle={styles.btnAddRestaurantText}
      onPress={() => navigation.navigate("AddRestaurant")}
    />
  );
}

const styles = StyleSheet.create({
  btnAddRestaurant: {
    marginTop: "110%",
    borderRadius: 0,
    backgroundColor: "#fff",
    borderTopWidth: 1,
    borderTopColor: "#e3e3e3",
    borderBottomWidth: 1,
    borderBottomColor: "#e3e3e3",
    paddingTop: 15,
    paddingBottom: 8,
  },
  btnAddRestaurantText: {
    fontWeight: "bold",
    color: "#00a680",
  },
  viewBody: {
    flex: 1,
  },
});
