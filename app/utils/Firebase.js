import firebase from "firebase/app";

/*const firebaseConfig = {
  apiKey: "AIzaSyBjnVzKrt0BJEWh6Dvva3u2WyBwfIZDVWU",
  authDomain: "apprestaurantes-42bbd.firebaseapp.com",
  databaseURL: "https://apprestaurantes-42bbd.firebaseio.com",
  projectId: "apprestaurantes-42bbd",
  storageBucket: "apprestaurantes-42bbd.appspot.com",
  messagingSenderId: "50748085649",
  appId: "1:50748085649:web:cdaa0bf3123ad5bd3ff630",
};*/

const firebaseConfig = {
  apiKey: "AIzaSyDStqSHXY3q0eelrMWxbVOAMYy420wvDK0",
  authDomain: "tenedores-ab733.firebaseapp.com",
  databaseURL: "https://tenedores-ab733.firebaseio.com",
  projectId: "tenedores-ab733",
  storageBucket: "tenedores-ab733.appspot.com",
  messagingSenderId: "237074586318",
  appId: "1:237074586318:web:400456cd28858df7cf8e9f",
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
