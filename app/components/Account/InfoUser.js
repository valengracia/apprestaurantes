import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { Avatar } from "react-native-elements";
import * as firebase from "firebase";
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";

export default function InfoUser(props) {
  const {
    //si queremos usar la variable hay que volver a declararla
    //userInfo,
    userInfo: { uid, displayName, email, photoURL }, //doble destructurin
    setReloadData,
    toastRef,
    setIsLoading,
    setTextLoading,
  } = props;
  //console.log(userInfo);

  const changeAvatar = async () => {
    const resultPermision = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    const resultPermisionCamera = resultPermision.permissions.cameraRoll.status;

    if (resultPermisionCamera === "denied") {
      console.log("Es necesario aceptar los permisos de la galeria");
      toastRef.current.show("Es necesario aceptar los permisos de la galería");
    } else {
      const result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3],
      });

      if (result.cancelled) {
        console.log("Has cerrado la galeria de imágenes");
        toastRef.current.show("Has cerrado la galería de imágenes");
      } else {
        //Nombre de la imagen será el UID del usuario. Si selecciona otra, la cambiará ya que es el mismo UID
        uploadImage(result.uri, uid).then(() => {
          console.log("Imagen subida correctamente");

          updatePhotoUrl(uid);
        });
      }
    }
  };

  const uploadImage = async (uri, nameImage) => {
    setTextLoading("Actualizando avatar");
    setIsLoading(true);
    const response = await fetch(uri);
    console.log(response);
    const blob = await response.blob();

    const ref = firebase.storage().ref().child(`Avatar/$(nameImage)`);
    return ref.put(blob);
  };

  const updatePhotoUrl = (uid) => {
    firebase
      .storage()
      .ref(`Avatar/${uid}`)
      .getDownloadURL()
      .then(async (result) => {
        const update = {
          photoURL: result,
        };
        await firebase.auth().currentUser.updateProfile(update);
        setReloadData(true);
        setIsLoading(false);
      })
      .catch(() => {
        console.log("Error al recuperar el avatar del servidor");
        toastRef.current.show("Error al recuperar el avatar del servidor");
      });
  };

  return (
    <View style={styles.viewUserInfo}>
      <Avatar
        rounded
        size="large"
        showEditButton
        onEditPress={changeAvatar}
        containerStyle={styles.userInfoAvatar}
        source={{
          uri: photoURL
            ? photoURL
            : "https://api.adorable.io/avatars/266/abott@adorable.png",
        }}
      />
      <View>
        <Text style={styles.displayName}>
          {displayName ? displayName : "Anónimo"}
        </Text>
        <Text>{email}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewUserInfo: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#f2f2f2",
    paddingTop: 30,
    paddingBottom: 30,
  },
  userInfoAvatar: {
    marginRight: 20,
  },
  displayName: {
    fontWeight: "bold",
  },
});
