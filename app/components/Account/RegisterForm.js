import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Input, Icon, Button } from "react-native-elements";
import { validateEmail } from "../../utils/Validation";
import * as firebase from "firebase";
import { withNavigation } from "react-navigation";
import Loading from "../Loading";

function RegisterForm(props) {
  const { toastRef, navigation } = props;

  const [hidePassword, setHidePassword] = useState(true); //si la contraseña está escondida, realiza la función setHidePassword
  const [hideRepeatPassword, setHideRepeatPassword] = useState(true);
  //Estados para guardar email, contraseña, repetir contraseña.
  const [isVisibleLoading, setIsVisibleLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [repeatPassword, setRepeatPassword] = useState("");

  const register = async () => {
    //async = método asíncrono.
    setIsVisibleLoading(true);
    if (!email || !password || !repeatPassword) {
      console.log("Todos los campos son obligatorios");
      toastRef.current.show("Todos los campos son obligatorios");
    } else {
      if (!validateEmail(email)) {
        console.log("El email no es correcto");
        toastRef.current.show("El email no es correcto");
      } else {
        if (password !== repeatPassword) {
          console.log("Las contraseñas no coinciden");
          toastRef.current.show("Las contraseñas no coinciden");
        } else {
          //función asíncrona
          await firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
              console.log("Usuario creado con éxito");
              toastRef.current.show("Usuario creado con éxito");
              navigation.navigate("MyAccount");
            })
            .catch((error) => {
              console.log("Error al crear la cuenta, intentelo más tarde");
              toastRef.current.show(
                "Error al crear la cuenta, intentelo más tard"
              );
              console.log(error);
            });
        }
      }
    }
    setIsVisibleLoading(false);
  };

  return (
    <View style={styles.formContainer} behavior="padding" enabled>
      <Input
        placeholder="Correo electrónico"
        containerStyle={styles.inputForm}
        onChange={(e) => setEmail(e.nativeEvent.text)} //esto es para coger el valor del campo y guardarlo en otro sitio.
        rightIcon={
          <Icon
            type="material-community"
            name="at"
            iconStyle={styles.iconRight}
          />
        }
      />
      <Input
        placeholder="Contraseña"
        password={true}
        secureTextEntry={hidePassword}
        containerStyle={styles.inputForm}
        onChange={(e) => setPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hidePassword ? "eye-outline" : "eye-off-outline"} //si hidepassword = true ponme el icono:"eye-outline" sino(:): "eye-off-outline"
            iconStyle={styles.iconRight}
            onPress={() => setHidePassword(!hidePassword)} //si escribimos la contraseña y queremos ver que hay escrito.
            //Para que funcione lo que hacemos es: haz el contrario de hidePassword. Si está oculto lo pondrá visible y viceversa.
          />
        }
      />
      <Input
        placeholder="Repetir contraseña"
        password={true}
        secureTextEntry={hideRepeatPassword}
        containerStyle={styles.inputForm}
        onChange={(e) => setRepeatPassword(e.nativeEvent.text)}
        rightIcon={
          <Icon
            type="material-community"
            name={hideRepeatPassword ? "eye-outline" : "eye-off-outline"}
            iconStyle={styles.iconRight}
            onPress={() => setHideRepeatPassword(!hideRepeatPassword)}
          />
        }
      />
      <Button
        title="Unirse"
        containerStyle={styles.btnContainerRegister}
        buttonStyle={styles.btnRegister}
        onPress={register}
      />
      <Loading text="Creando cuenta" isVisible={isVisibleLoading} />
    </View>
  );
}

export default withNavigation(RegisterForm);

const styles = StyleSheet.create({
  formContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
  },
  inputForm: {
    width: "100%",
    marginTop: 20,
  },
  iconRight: {
    color: "#c1c1c1",
  },
  btnContainerRegister: {
    marginTop: 20,
    width: "95%",
  },
  btnRegister: {
    backgroundColor: "#00a680",
  },
});
