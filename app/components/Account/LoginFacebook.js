import React from "react";
import { SocialIcon } from "react-native-elements";

export default function LoginFacebook() {
  const login = () => {
    console.log("Iniciando sesión con facebook");
  };

  return (
    <SocialIcon
      title="Iniciar sesion con Facebook"
      button
      type="facebook"
      onPress={login}
    />
  );
}
